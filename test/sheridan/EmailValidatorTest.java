package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {

	@Test
	public void testHasValidFormat() {
		boolean result = EmailValidator.isValidEmail("aaron@email.com");
		assertTrue("invalid email format", result);
	}
	
	@Test
	public void testHasValidFormatBoundaryIn() {
		boolean result = EmailValidator.isValidEmail("a@a.a");
		assertTrue("invalid email format", result);
	}
	
	@Test
	public void testHasValidFormatBoundaryOut() {
		boolean result = EmailValidator.isValidEmail("@.");
		assertFalse("invalid email format", result);
	}
	
	@Test
	public void testHasValidFormatException() {
		boolean result = EmailValidator.isValidEmail("");
		assertFalse("invalid email format", result);
	}
	
	@Test
	public void testHasAt() {
		assertTrue("invalid email missing @", EmailValidator.hasAtEmail("a@a.a"));
	}
	
	@Test
	public void testHasAtBoundaryIn() {
		assertTrue("invalid email missing @", EmailValidator.hasAtEmail("@"));
	}
	
	@Test
	public void testHasAtException() {
		assertFalse("invalid email missing @", EmailValidator.hasAtEmail(""));
	}
	
	@Test
	public void testIsAccountValid() {
		boolean result = EmailValidator.isValidAccount("aaron@email.com");
		assertTrue("invalid account", result);
	}
	
	@Test
	public void testIsAccountValidBoundaryIn() {
		boolean result = EmailValidator.isValidAccount("aaa@a.com");
		assertTrue("invalid account", result);
	}
	
	@Test
	public void testIsAccountValidBoundaryOut() {
		boolean result = EmailValidator.isValidAccount("aa@a.com");
		assertFalse("invalid account", result);
	}
	
	@Test
	public void testIsAccountValidException() {
		boolean result = EmailValidator.isValidAccount("@a.com");
		assertFalse("invalid account", result);
	}
	
	@Test
	public void testIsDomainValid() {
		boolean result = EmailValidator.isValidDomain("aaron@email.com");
		assertTrue("invalid account", result);
	}
	
	@Test
	public void testIsDomainValidBoundaryIn() {
		boolean result = EmailValidator.isValidDomain("aaron@ab1.com");
		assertTrue("invalid account", result);
	}
	
	@Test
	public void testIsDomainValidBoundaryOut() {
		boolean result = EmailValidator.isValidDomain("aaron@ab.com");
		assertFalse("invalid account", result);
	}
	
	@Test
	public void testIsDomainValidException() {
		boolean result = EmailValidator.isValidDomain("aaron@a.com");
		assertFalse("invalid account", result);
	}
	
	@Test
	public void testIsExtensionValid() {
		boolean result = EmailValidator.isValidExt("aaron@email.com");
		assertTrue("invalid account", result);
	}
	
	@Test
	public void testIsExtensionValidBoundaryIn() {
		boolean result = EmailValidator.isValidExt("aaron@email.ca");
		assertTrue("invalid account", result);
	}
	
	@Test
	public void testIsExtensionValidBoundaryOut() {
		boolean result = EmailValidator.isValidExt("aaron@email.c");
		assertFalse("invalid account", result);
	}
	
	@Test
	public void testIsExtensionValidException() {
		boolean result = EmailValidator.isValidExt("aaron@email.1");
		assertFalse("invalid account", result);
	}


}
