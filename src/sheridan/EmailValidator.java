package sheridan;

public class EmailValidator {
	
	public static boolean isValidEmail(String email) {
		return email != null && email.matches("^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$");
	}
	
	public static boolean hasAtEmail(String email) {
		return email.matches("^(.*)@(.*)$");
	}
	
	private static int MIN_ACC_LENGTH = 3;
	
	public static boolean isValidAccount(String email) {
		String acc = email.split("@")[0];
		if (acc.isEmpty()){
			return false;
		}
		if (Character.isDigit(acc.charAt(0))) {
			return false;
		}
		int letters = 0;
		for (int i=0; i < acc.trim().length(); i++) {
			if (Character.isAlphabetic(acc.trim().charAt(i)) && Character.isLowerCase(acc.trim().charAt(i))) {
				letters++;
			}
		}
		return letters >= MIN_ACC_LENGTH;
	}
	
	private static int MIN_DOM_LENGTH = 3;
	
	public static boolean isValidDomain(String email) {
		email = email.substring(email.indexOf("@") + 1);
		String dom = email.substring(0, email.indexOf("."));
		
		int count = 0;
		for (int i=0; i < dom.trim().length(); i++) {
			
			if (Character.isDigit(dom.charAt(i)) || Character.isLowerCase(dom.charAt(i))) {
				count++;
			}
		}
		return count >= MIN_DOM_LENGTH;
	}

	private static int MIN_EXT_LENGTH = 2;
	
	public static boolean isValidExt(String email) {
		String dom = email.split("\\.")[email.split("\\.").length - 1];

		int count = 0;
		for (int i=0; i < dom.trim().length(); i++) {
			if (Character.isDigit(dom.trim().charAt(i))) {
				return false;
			}
			else if (Character.isLowerCase(dom.trim().charAt(i))) {
				count++;
			}
		}
		//checks if valid number of digits
		return count >= MIN_EXT_LENGTH;
	}


}
